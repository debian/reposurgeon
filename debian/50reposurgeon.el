;; -*-emacs-lisp-*-
;;
;; Emacs startup file for the Debian reposurgeon package

(if (not (file-exists-p "/usr/share/emacs/site-lisp/reposurgeon-mode.el"))
    (message "Package reposurgeon removed but not purged.  Skipping setup.")

  (autoload 'reposurgeon-mode "reposurgeon-mode"
    "Major mode for editing reposurgeon comment dumps." t)
  (setq auto-mode-alist
	(cons '("\\.fi$" . reposurgeon-mode) auto-mode-alist)))
